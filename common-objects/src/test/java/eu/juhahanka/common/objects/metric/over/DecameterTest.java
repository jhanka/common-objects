package eu.juhahanka.common.objects.metric.over;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;
import eu.juhahanka.test.helpers.AbstractMetricObjectTest;

public class DecameterTest extends AbstractMetricObjectTest {

	@Override
	protected String getExpectedToString() {
		return "2.22dam";
	}

	@Override
	protected Class<? extends AbstractMetricObject> getTestableClass() {
		return Decameter.class;
	}

}
