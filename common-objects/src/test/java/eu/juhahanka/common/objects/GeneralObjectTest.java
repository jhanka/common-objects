package eu.juhahanka.common.objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Test;

public class GeneralObjectTest {

	GeneralObject object = new GeneralObject();

	@Test
	public void testHashCode() throws Exception {
		assertEquals(new GeneralObject().hashCode(), object.hashCode());
		assertNotSame("".hashCode(), object.hashCode());
	}

	@Test
	public void testEquals() throws Exception {
		assertFalse(object.equals(""));
		assertTrue(new GeneralObject().equals(object));
	}

	@Test
	public void testToString() throws Exception {
		assertEquals(ToStringBuilder.reflectionToString(object), object.toString());
	}

	private void assertStyle(Object style, ToStringStyle expectedStyle) {
		assertEquals(ToStringBuilder.reflectionToString(style, expectedStyle), style.toString());
	}

	@Test
	public void testAllToStringStyles() throws Exception {
		assertStyle(new MultiStyle(), ToStringStyle.MULTI_LINE_STYLE);
		assertStyle(new JSONStyle(), ToStringStyle.JSON_STYLE);
		assertStyle(new NoClassNameStyle(), ToStringStyle.NO_CLASS_NAME_STYLE);
		assertStyle(new NoFieldsNamesStyle(), ToStringStyle.NO_FIELD_NAMES_STYLE);
		assertStyle(new ShortStyle(), ToStringStyle.SHORT_PREFIX_STYLE);
		assertStyle(new SimpleStyle(), ToStringStyle.SIMPLE_STYLE);
	}

	@ToPrintStyle(json = true)
	class JSONStyle extends GeneralObject {
		private static final long serialVersionUID = 1L;
	}

	@ToPrintStyle(multi_line = true)
	class MultiStyle extends GeneralObject {
		private static final long serialVersionUID = 1L;
	}

	@ToPrintStyle(noClassName = true)
	class NoClassNameStyle extends GeneralObject {
		private static final long serialVersionUID = 1L;
	}

	@ToPrintStyle(noFieldNames = true)
	class NoFieldsNamesStyle extends GeneralObject {
		private static final long serialVersionUID = 1L;
	}

	@ToPrintStyle(shortPrefix = true)
	class ShortStyle extends GeneralObject {
		private static final long serialVersionUID = 1L;
	}

	@ToPrintStyle(simple = true)
	class SimpleStyle extends GeneralObject {
		private static final long serialVersionUID = 1L;
	}

}
