package eu.juhahanka.common.objects.metric.unit;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SITest {

	@Test
	public void testSIUnits() {
		assertSI("Yotta", "Y", Math.pow(10, 24));
		assertSI("Zetta", "Z", Math.pow(10, 21));
		assertSI("Exa", "E", Math.pow(10, 18));
		assertSI("Peta", "P", Math.pow(10, 15));
		assertSI("Tera", "T", Math.pow(10, 12));
		assertSI("Giga", "G", Math.pow(10, 9));
		assertSI("Mega", "M", Math.pow(10, 6));
		assertSI("Kilo", "k", Math.pow(10, 3));
		assertSI("hecto", "h", Math.pow(10, 2));
		assertSI("deca", "da", Math.pow(10, 1));
		assertSI("meter", "m", 1);
		assertSI("deci", "d", Math.pow(10, -1));
		assertSI("centi", "c", Math.pow(10, -2));
		assertSI("milli", "m", Math.pow(10, -3));
		assertSI("micro", "µ", Math.pow(10, -6));
		assertSI("nano", "n", Math.pow(10, -9));
		assertSI("pico", "p", Math.pow(10, -12));
		assertSI("femto", "f", Math.pow(10, -15));
		assertSI("atto", "a", Math.pow(10, -18));
		assertSI("zepto", "z", Math.pow(10, -21));
		assertSI("yocto", "y", Math.pow(10, -24));
	}

	private void assertSI(String si, String expectedDescription, double expectedFactor) {
		assertEquals("\n" + si + " description NOT AS expected!", expectedDescription, SI.valueOf(si).getDescription());
		assertEquals("\n'" + si + "' factor NOT AS expected!", expectedFactor, SI.valueOf(si).getFactor(), 0.0);
	}

	@Test
	public void testSIConvert() {
		assertEquals(200.0, SI.convert(0.002, SI.Kilo, SI.centi), 0.0);
	}

}
