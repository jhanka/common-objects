package eu.juhahanka.common.objects.metric.under;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.juhahanka.common.objects.metric.Meter;
import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;
import eu.juhahanka.test.helpers.AbstractMetricObjectTest;

public class CentimeterTest extends AbstractMetricObjectTest {

	@Override
	protected Class<? extends AbstractMetricObject> getTestableClass() {
		return Centimeter.class;
	}

	protected String getExpectedToString() {
		return "2.22cm";
	}
	@Test
	public void test(){
		assertEquals(new Meter(2),new Centimeter(200).convertTo(Meter.class));
	}
}