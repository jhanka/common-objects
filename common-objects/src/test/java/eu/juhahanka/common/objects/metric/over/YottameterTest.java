package eu.juhahanka.common.objects.metric.over;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;
import eu.juhahanka.test.helpers.AbstractMetricObjectTest;

public class YottameterTest extends AbstractMetricObjectTest {

	@Override
	protected Class<? extends AbstractMetricObject> getTestableClass() {
		return Yottameter.class;
	}

	protected String getExpectedToString() {
		return "2.22Ym";
	}
}
