package eu.juhahanka.common.objects.metric.under;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;
import eu.juhahanka.test.helpers.AbstractMetricObjectTest;

public class DecimeterTest extends AbstractMetricObjectTest {

	@Override
	protected Class<? extends AbstractMetricObject> getTestableClass() {
		return Decimeter.class;
	}

	protected String getExpectedToString() {
		return "2.22dm";
	}
}