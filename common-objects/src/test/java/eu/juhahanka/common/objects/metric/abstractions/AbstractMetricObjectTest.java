package eu.juhahanka.common.objects.metric.abstractions;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import eu.juhahanka.common.objects.metric.Meter;
import eu.juhahanka.common.objects.metric.under.Centimeter;

@RunWith(MockitoJUnitRunner.class)
public class AbstractMetricObjectTest {

	@Spy
	private AbstractMetricObject mo;

	@Before
	public void setup() {
		mo.setValue(2.001234);
	}

	@Test
	public void testGetAndSetValue() throws Exception {
		assertEquals(2.001234, mo.getValue(), 0.0);
	}

	@Test
	public void testToString() throws Exception {
		mo.setValue(2.109002000099988);
		assertEquals("2.11m", mo.toString());
		assertEquals("2.109002000099988m", mo.toStringExact());
	}

	@Test
	public void testConvertTo() throws Exception {
		assertEquals(new Centimeter(200.1234), mo.convertTo(Centimeter.class));
	}

	@Test
	public void testPlus() throws Exception {
		assertEquals(new Centimeter(220), new Centimeter(120).plus(new Meter(1)));
	}

	@Test
	public void testMinus() throws Exception {
		assertEquals(new Centimeter(20), new Centimeter(120).minus(new Meter(1)));
	}

	@Test
	public void testDivision() throws Exception {
		assertEquals(new Centimeter(1), new Centimeter(200).division(new Meter(2)));
	}

	@Test
	public void testMultiply() throws Exception {
		assertEquals(new Centimeter(400), new Centimeter(200).multiply(new Meter(0.02)));
	}

}
