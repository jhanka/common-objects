package eu.juhahanka.common.objects.metric;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;
import eu.juhahanka.test.helpers.AbstractMetricObjectTest;

public class MeterTest extends AbstractMetricObjectTest {

	@Override
	protected Class<? extends AbstractMetricObject> getTestableClass() {
		return Meter.class;
	}

	protected String getExpectedToString() {
		return "2.22m";
	}
}
