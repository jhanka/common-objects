package eu.juhahanka.test.helpers;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public abstract class AbstractMetricObjectTest {

	public <T extends AbstractMetricObject> T getDefaultConstructor(Class<T> clazz)
			throws InstantiationException, IllegalAccessException {
		return clazz.newInstance();
	}

	protected abstract String getExpectedToString();
	protected abstract Class<? extends AbstractMetricObject> getTestableClass();

	@Test
	public void testCallObjectWithConstratorWithNumberParameter()
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
			NoSuchMethodException, SecurityException, ClassNotFoundException {
		Class.forName(getTestableClass().getName()).getConstructor(Number.class).newInstance(0.0);
	}

	@Test
	public void testToString() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException{
		double value = 2.217;
		assertEquals("\nInvoked Constructor with parameter '"+value+"'. We are expecting rounded 2.22 value with correct SI unit.\ne.g. new Meter(2.217) should printout 2.22m.\n",getExpectedToString(),Class.forName(getTestableClass().getName()).getConstructor(Number.class).newInstance(value).toString());
	}
	

	@Test
	public void testDefaultConstructor() throws Exception {
		AbstractMetricObject object = getDefaultConstructor(getTestableClass());
		object.setValue(0.0);
		assertEquals(object, getDefaultConstructor(getTestableClass()));
	}


}
