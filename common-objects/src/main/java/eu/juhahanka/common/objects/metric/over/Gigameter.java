package eu.juhahanka.common.objects.metric.over;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Gigameter extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Gigameter() {
		super();
	}

	public Gigameter(Number value) {
		super(value);
	}

}
