package eu.juhahanka.common.objects.metric.under;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Nanometer extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Nanometer() {
		super();
	}

	public Nanometer(Number value) {
		super(value);
	}

}
