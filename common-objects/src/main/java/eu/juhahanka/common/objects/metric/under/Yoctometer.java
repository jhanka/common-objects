package eu.juhahanka.common.objects.metric.under;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Yoctometer extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Yoctometer() {
		super();
	}

	public Yoctometer(Number value) {
		super(value);
	}

}
