package eu.juhahanka.common.objects.metric.abstractions;

public interface Convertable {

	public <T extends AbstractMetricObject> T convertTo(Class<T> clazz);

}
