package eu.juhahanka.common.objects.metric.over;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Decameter  extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Decameter() {
		super();
	}
	
	public Decameter(Number value){
		super(value);
	}
	
}
