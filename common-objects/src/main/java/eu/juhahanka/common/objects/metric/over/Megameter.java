package eu.juhahanka.common.objects.metric.over;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Megameter extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Megameter() {
		super();
	}

	public Megameter(Number value) {
		super(value);
	}

}
