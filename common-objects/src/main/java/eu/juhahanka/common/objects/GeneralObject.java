package eu.juhahanka.common.objects;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlTransient
@ToPrintStyle
public class GeneralObject implements Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this, false);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, false);
	}

	@Override
	public String toString() {
		ToStringStyle style = ToStringStyle.DEFAULT_STYLE;
		ToPrintStyle printStyle = getClass().getAnnotation(ToPrintStyle.class);
		if (printStyle.multi_line()) {
			style = ToStringStyle.MULTI_LINE_STYLE;
		} else if (printStyle.json()) {
			style = ToStringStyle.JSON_STYLE;
		} else if (printStyle.noClassName()) {
			style = ToStringStyle.NO_CLASS_NAME_STYLE;
		} else if (printStyle.noFieldNames()) {
			style = ToStringStyle.NO_FIELD_NAMES_STYLE;
		} else if (printStyle.shortPrefix()) {
			style = ToStringStyle.SHORT_PREFIX_STYLE;
		} else if (printStyle.simple()) {
			style = ToStringStyle.SIMPLE_STYLE;
		}
		return ToStringBuilder.reflectionToString(this, style);
	}

}
