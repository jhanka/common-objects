package eu.juhahanka.common.objects.metric.under;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Micrometer extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Micrometer() {
		super();
	}

	public Micrometer(Number value) {
		super(value);
	}

}
