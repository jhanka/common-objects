package eu.juhahanka.common.objects.metric.under;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Centimeter extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Centimeter() {
		super();
	}

	public Centimeter(Number value) {
		super(value);
	}

}
