package eu.juhahanka.common.objects.metric.under;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Picometer extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Picometer() {
		super();
	}

	public Picometer(Number value) {
		super(value);
	}

}
