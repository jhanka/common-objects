package eu.juhahanka.common.objects.metric.over;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Kilometer extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Kilometer() {
		super();
	}

	public Kilometer(Number value) {
		super(value);
	}

}
