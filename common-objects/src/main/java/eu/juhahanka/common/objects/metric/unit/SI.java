package eu.juhahanka.common.objects.metric.unit;

import eu.juhahanka.common.objects.metric.Meter;
import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;
import eu.juhahanka.common.objects.metric.over.Decameter;
import eu.juhahanka.common.objects.metric.over.Exameter;
import eu.juhahanka.common.objects.metric.over.Gigameter;
import eu.juhahanka.common.objects.metric.over.Hectometer;
import eu.juhahanka.common.objects.metric.over.Kilometer;
import eu.juhahanka.common.objects.metric.over.Megameter;
import eu.juhahanka.common.objects.metric.over.Petameter;
import eu.juhahanka.common.objects.metric.over.Terameter;
import eu.juhahanka.common.objects.metric.over.Yottameter;
import eu.juhahanka.common.objects.metric.over.Zettameter;
import eu.juhahanka.common.objects.metric.under.Attometer;
import eu.juhahanka.common.objects.metric.under.Centimeter;
import eu.juhahanka.common.objects.metric.under.Decimeter;
import eu.juhahanka.common.objects.metric.under.Femtometer;
import eu.juhahanka.common.objects.metric.under.Micrometer;
import eu.juhahanka.common.objects.metric.under.Millimeter;
import eu.juhahanka.common.objects.metric.under.Nanometer;
import eu.juhahanka.common.objects.metric.under.Picometer;
import eu.juhahanka.common.objects.metric.under.Yoctometer;
import eu.juhahanka.common.objects.metric.under.Zeptometer;

public enum SI {
	Yotta("Y",Math.pow(10, 24),Yottameter.class), 
	Zetta("Z", Math.pow(10, 21),Zettameter.class), 
	Exa("E", Math.pow(10, 18),Exameter.class), 
	Peta("P", Math.pow(10,15),Petameter.class), 
	Tera("T", Math.pow(10, 12),Terameter.class), 
	Giga("G", Math.pow(10, 9),Gigameter.class), 
	Mega("M", Math.pow(10, 6),Megameter.class),
	Kilo("k",Math.pow(10, 3),Kilometer.class),
	hecto("h", Math.pow(10, 2),Hectometer.class),
	deca("da", Math.pow(10, 1),Decameter.class),
	meter("m",1,Meter.class), 
	deci("d", Math.pow(10, -1),Decimeter.class), 
	centi("c", Math.pow(10, -2),Centimeter.class), 
	milli("m",Math.pow(10, -3),Millimeter.class), 
	micro("µ", Math.pow(10, -6),Micrometer.class), 
	nano("n",Math.pow(10, -9),Nanometer.class), 
	pico("p", Math.pow(10, -12),Picometer.class), 
	femto("f",Math.pow(10, -15),Femtometer.class), 
	atto("a", Math.pow(10, -18),Attometer.class), 
	zepto("z",Math.pow(10, -21),Zeptometer.class), 
	yocto("y", Math.pow(10, -24),Yoctometer.class),
	UNKNOWN("", 1,AbstractMetricObject.class);

	private final String description;
	private final double factor;
	private final Class<? extends AbstractMetricObject> implementationClass;

	SI(String description, double factor, Class<? extends AbstractMetricObject> implementationClass) {
		this.description = description;
		this.factor = factor;
		this.implementationClass = implementationClass;
	}
	
	public Class<? extends AbstractMetricObject> getImplementingClass(){
		return this.implementationClass;
	}

	public String getDescription() {
		return description;
	}

	public double getFactor() {
		return factor;
	}

	public static double convert(double fromValue, SI from, SI to) {
		return (fromValue * from.factor) / to.factor;
	}

	public static SI get(Class<? extends AbstractMetricObject> clazz) {
		for (SI si : values()) {
			Class<? extends AbstractMetricObject> implementingClass = si.getImplementingClass();
			if(implementingClass.equals(clazz)){
				return si;
			}
		}
		return UNKNOWN;
	}
}
