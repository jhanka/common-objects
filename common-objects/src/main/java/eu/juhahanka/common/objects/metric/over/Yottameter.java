package eu.juhahanka.common.objects.metric.over;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Yottameter extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Yottameter() {
		super();
	}

	public Yottameter(Number value) {
		super(value);
	}

}
