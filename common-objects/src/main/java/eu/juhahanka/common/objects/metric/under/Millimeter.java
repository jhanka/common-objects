package eu.juhahanka.common.objects.metric.under;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Millimeter extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Millimeter() {
		super();
	}

	public Millimeter(Number value) {
		super(value);
	}

}
