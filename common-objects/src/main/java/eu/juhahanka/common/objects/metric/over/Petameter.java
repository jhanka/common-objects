package eu.juhahanka.common.objects.metric.over;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Petameter extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Petameter() {
		super();
	}

	public Petameter(Number value) {
		super(value);
	}

}
