package eu.juhahanka.common.objects.metric.under;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Femtometer extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Femtometer() {
		super();
	}

	public Femtometer(Number value) {
		super(value);
	}

}
