package eu.juhahanka.common.objects;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = { ElementType.TYPE })
@Retention(value = RetentionPolicy.RUNTIME)
@Inherited
public @interface ToPrintStyle {

	boolean json() default false;

	boolean multi_line() default false;;

	boolean noClassName() default false;

	boolean noFieldNames() default false;

	boolean shortPrefix() default false;

	boolean simple() default false;

}
