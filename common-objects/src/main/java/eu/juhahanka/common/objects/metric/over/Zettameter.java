package eu.juhahanka.common.objects.metric.over;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Zettameter extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Zettameter() {
		super();
	}

	public Zettameter(Number value) {
		super(value);
	}

}
