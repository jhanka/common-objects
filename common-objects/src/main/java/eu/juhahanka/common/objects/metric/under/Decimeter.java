package eu.juhahanka.common.objects.metric.under;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Decimeter extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Decimeter() {
		super();
	}

	public Decimeter(Number value) {
		super(value);
	}

}
