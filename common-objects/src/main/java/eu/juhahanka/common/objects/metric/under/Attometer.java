package eu.juhahanka.common.objects.metric.under;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Attometer extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Attometer() {
		super();
	}

	public Attometer(Number value) {
		super(value);
	}

}
