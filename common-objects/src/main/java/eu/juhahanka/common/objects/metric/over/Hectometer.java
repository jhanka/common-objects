package eu.juhahanka.common.objects.metric.over;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Hectometer extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Hectometer() {
		super();
	}

	public Hectometer(Number value) {
		super(value);
	}

}
