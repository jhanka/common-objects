package eu.juhahanka.common.objects.metric.abstractions;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.juhahanka.common.objects.GeneralObject;
import eu.juhahanka.common.objects.metric.unit.SI;

public abstract class AbstractMetricObject extends GeneralObject implements Convertable{

	private static final long serialVersionUID = 1L;
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private transient static String DECIMAL_PATTERN = "#.##";
	private transient static final DecimalFormatSymbols FORMAT_SYMBOLS = DecimalFormatSymbols
			.getInstance(Locale.ENGLISH);
	private transient static final DecimalFormat FORMATER = new DecimalFormat(DECIMAL_PATTERN, FORMAT_SYMBOLS);
	private transient static final DecimalFormat EXACT_FORMATER = new DecimalFormat("#.##############################",
			FORMAT_SYMBOLS);

	protected double value;

	public AbstractMetricObject() {
		this(0.0);
	}

	public AbstractMetricObject(Number value) {
		this.value = value.doubleValue();
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public AbstractMetricObject plus(AbstractMetricObject abstractMetricObject){
		this.value += abstractMetricObject.convertTo(getClass()).getValue();
		return this;
	}

	public AbstractMetricObject minus(AbstractMetricObject abstractMetricObject){
		this.value -= abstractMetricObject.convertTo(getClass()).getValue();
		return this;
	}

	public AbstractMetricObject division(AbstractMetricObject abstractMetricObject){
		this.value /= abstractMetricObject.convertTo(getClass()).getValue();
		return this;
	}

	public AbstractMetricObject multiply(AbstractMetricObject abstractMetricObject){
		this.value *= abstractMetricObject.convertTo(getClass()).getValue();
		return this;
	}

	@Override
	public String toString() {
		return FORMATER.format(value) + addMeterMarkIfNotMeterUnit();
	}

	private String addMeterMarkIfNotMeterUnit() {
		return SI.get(getClass()).getDescription() + (SI.meter.equals(SI.get(getClass())) ? "" : "m");
	}

	public String toStringExact() {
		return EXACT_FORMATER.format(value) + addMeterMarkIfNotMeterUnit();
	}

	@Override
	public <T extends AbstractMetricObject> T convertTo(Class<T> clazz){
		T object;
		try {
			object = clazz.newInstance();
			object.setValue(SI.convert(value, SI.get(getClass()),SI.get(clazz)));
			logger.debug("{} -> {}",this,object);
			return object;
		} catch (InstantiationException | IllegalAccessException e) {
			throw new IllegalStateException("Impossible state this can't be reached! So don't worry about testing.");
		}
	}
}
