package eu.juhahanka.common.objects.metric;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Meter extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Meter() {
		super();
	}

	public Meter(Number value) {
		super(value);
	}

}
