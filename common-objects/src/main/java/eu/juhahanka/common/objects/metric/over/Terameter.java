package eu.juhahanka.common.objects.metric.over;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Terameter extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Terameter() {
		super();
	}

	public Terameter(Number value) {
		super(value);
	}

}
