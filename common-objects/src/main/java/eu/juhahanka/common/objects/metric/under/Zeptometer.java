package eu.juhahanka.common.objects.metric.under;

import eu.juhahanka.common.objects.metric.abstractions.AbstractMetricObject;

public class Zeptometer extends AbstractMetricObject {

	private static final long serialVersionUID = 1L;

	public Zeptometer() {
		super();
	}

	public Zeptometer(Number value) {
		super(value);
	}

}
